#include "cute.h"

#include <iostream>
#include <fstream>

#include "FileMgr.h"
#include "TestFileMgr.h"
#include "CommonTestSettings.h"
#include "CinManip.h"


using namespace std;

static const string TestFilesDir="TestFiles/FileMgr";

void testQueryFunctions() {
	cout << "Info: the UT currently does not test the support of symbolic links" << endl;
	cout << "Testing directoryExists..." << endl;
	ASSERT(FileMgr::directoryExists(TestFilesDir));
	ASSERT(FileMgr::directoryExists(TestFilesDir+"/aDirectory"));
	ASSERT(!FileMgr::directoryExists(TestFilesDir+"/regularFile.txt"));
	ASSERT(!FileMgr::directoryExists("inexistentDir122345azerty"));

	cout << "Testing fileExists..." << endl;
	ASSERT(FileMgr::fileExists(TestFilesDir+"/regularFile.txt"));
	ASSERT(FileMgr::isRegularFile(TestFilesDir+"/regularFile.txt"));
	ASSERT(!FileMgr::isRegularFile(TestFilesDir+"/aDirectory"));
	ASSERT(!FileMgr::isRegularFile(TestFilesDir+"/inexistentPath"));
}

void testDirectoryListing() {
	vector<string> entries;
	FileMgr::listDirectory(entries, TestFilesDir, ".aaa", true /*limit to regular files*/ );
	cout << "*.aaa: Found " << entries.size() << " entries" << endl;
	ASSERT(entries.size()==3);

	entries.clear();
	FileMgr::listDirectory(entries, TestFilesDir, ".bbb", true /*limit to regular files*/ );
	cout << "*.bbb: Found " << entries.size() << " entries" << endl;
	ASSERT(entries.size()==2);

	entries.clear();
	FileMgr::listDirectory(entries, TestFilesDir, ".inexistent", true /*limit to regular files*/ );
	cout << "*.inexistant: Found " << entries.size() << " entries" << endl;
	ASSERT(entries.size()==0);

	entries.clear();
	FileMgr::listDirectory(entries, "inexistentDir", ".aaa", true /*limit to regular files*/ );
	cout << "Inexistent dir: Found " << entries.size() << " entries" << endl;
	ASSERT(entries.size()==0);

}

void testDirectoryCreationDeletion()
{
	const string tmpDirForTest=TestFilesDir+"/tmpForTest";
	if(FileMgr::directoryExists(tmpDirForTest))
	{
		cout << "*****" << endl
			 << "***** Directory '" << tmpDirForTest
			 	 << "' should not exist for the test to run." << endl
			 << "***** Remove manually and restart test." << endl
			 << "*****" << endl;
		ASSERT(!"Test precondition not OK");
	}

	//At this stage, directory does not exist. Check it can be silently removed again.
	ASSERT(FileMgr::removeDirectory(tmpDirForTest, false, true));
	// Check if fails if silentIfInexistent is false;
	ASSERT(!FileMgr::removeDirectory(tmpDirForTest, false, false));

	// Create single level directory
	ASSERT(FileMgr::createDirectory(tmpDirForTest,true, false));
	// Create again silently or not
	ASSERT(FileMgr::createDirectory(tmpDirForTest,false, true));
	ASSERT(!FileMgr::createDirectory(tmpDirForTest,false, false));

	// Create multi-level directory inside existing one, silently or not
	// First creation succeeds
	ASSERT(FileMgr::createDirectory(tmpDirForTest+"/a/b/c",false, false));
	ASSERT(FileMgr::directoryExists(tmpDirForTest+"/a/b/c"));
	// Next succeeds: already exists and silently ignored
	ASSERT(FileMgr::createDirectory(tmpDirForTest+"/a/b/c",false, true));
	// Next one fails.
	ASSERT(!FileMgr::createDirectory(tmpDirForTest+"/a/b/c",false, false));

	// Check creation
	ASSERT(FileMgr::directoryExists(tmpDirForTest+"/a/b/c"));
	// Single level deletion (a/b/c)
	ASSERT(FileMgr::removeDirectory(tmpDirForTest+"/a/b/c", false, false));
	// Check parent a/b still exists
	ASSERT(FileMgr::directoryExists(tmpDirForTest+"/a/b"));
	// Remove one more level
	ASSERT(FileMgr::removeDirectory(tmpDirForTest+"/a/b", false, false));
	// Check parent a still exists
	ASSERT(FileMgr::directoryExists(tmpDirForTest+"/a"));

	// Remove one more level
	ASSERT(FileMgr::removeDirectory(tmpDirForTest+"/a", false, false));

	// Remove root.
	ASSERT(FileMgr::removeDirectory(tmpDirForTest, false, true));
	// Check deletion
	ASSERT(!FileMgr::directoryExists(tmpDirForTest));

	cout << "TEST TO BE COMPLETED" << endl;

	// delete non-empty directories?

}

void createFileIfInexistent(const string& newFile) {
	if (!FileMgr::fileExists(newFile)) {
		std::ofstream outfile(newFile);
		outfile << "Just a test file (regenerated at each run)."
				<< std::endl;
		outfile.close();
	}
}

void InitTestOfMoveFile(const string& fileToMove, const string& existingDir,
		const string& inexistentDir, const string& movedFile, const string& inexistentFile) {
	cout << "    Initializing test..." << endl;
	// create a file to move
	createFileIfInexistent(fileToMove);

	// Remove destination file.
	ASSERT(FileMgr::deleteFile(movedFile, true));
	ASSERT(FileMgr::deleteFile(inexistentFile, true));
	if (FileMgr::fileExists(inexistentDir)) { // some tests created file instead of dir
		FileMgr::deleteFile(inexistentDir, true);
	}
	if (FileMgr::directoryExists(inexistentDir)) {
		ASSERT(FileMgr::deleteFiles(inexistentDir,"*",false));
		ASSERT(FileMgr::removeDirectory(inexistentDir));
	}

	// check destDir exists and inexistentDir does not
	ASSERTM(string("Directory '") + existingDir + "' should exist!",
			FileMgr::directoryExists(existingDir));
	ASSERTM(string("Directory '") + inexistentDir + "' should NOT exist!",
			!FileMgr::directoryExists(inexistentDir));
	ASSERTM(string("File '") + fileToMove + "' should exist!",
			FileMgr::fileExists(fileToMove));
	ASSERTM(string("File '") + inexistentFile + "' should exist!",
			!FileMgr::fileExists(inexistentFile));
	cout << "    Test initialized" << endl;
}

void TestMoveFile(){
	string testDir="TestFiles/FileMgr/moveTests";
	string fileToMove = testDir + "/fileToMove.txt";
	string inexistentFile= testDir + "/inexistentFile";
	string existingDir=testDir+"/destDir";
	string inexistentDir=testDir+"/inexistentDir";
	string movedFile = existingDir+ "/fileToMove.txt";
	string movedFileToInexistentDir = inexistentDir + "/fileToMove.txt";


	// 1. move existent file to existent empty directory.
	cout << "1. Moving existent file to existent empty directory. No error/msg expected" << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	cout << "   ...testing..." << endl;
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, true, true));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, false, true));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, true, false));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, false, false));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));

	// 2. move inexistent file to existent empty directory.
	cout << "2. Moving inexistant file, not interactive" << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(!FileMgr::moveFile(inexistentFile, existingDir, false, false));
#ifdef INCLUDE_INTERACTIVE_TESTS
	cout << "Moving inexistant file, interactive" << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(!FileMgr::moveFile(inexistentFile, existingDir, true, true));
#endif

	// 3. move existent file to inexistent directory.
	cout << "3. Moving existant file to inexistent directory. No error expected." << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, inexistentDir, false, false));
	ASSERT(FileMgr::fileExists(movedFileToInexistentDir));
	ASSERT(!FileMgr::fileExists(fileToMove));
	ASSERT(FileMgr::directoryExists(inexistentDir));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, inexistentDir, true, false));
	ASSERT(FileMgr::fileExists(movedFileToInexistentDir));
	ASSERT(!FileMgr::fileExists(fileToMove));
	ASSERT(FileMgr::directoryExists(inexistentDir));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, inexistentDir, false, true));
	ASSERT(FileMgr::fileExists(movedFileToInexistentDir));
	ASSERT(!FileMgr::fileExists(fileToMove));
	ASSERT(FileMgr::directoryExists(inexistentDir));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	ASSERT(FileMgr::moveFile(fileToMove, inexistentDir, true, true));
	ASSERT(FileMgr::fileExists(movedFileToInexistentDir));
	ASSERT(!FileMgr::fileExists(fileToMove));
	ASSERT(FileMgr::directoryExists(inexistentDir));

	// 4. move existent file to existent directory with file to overwrite.
	cout << "4. Moving existent file to existent directory with file to overwrite." <<endl;
	cout << "   a. Silent overwrite: No error expected." << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	createFileIfInexistent(movedFile);
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, true, true));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	createFileIfInexistent(movedFile);
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(FileMgr::moveFile(fileToMove, existingDir, true, false));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::fileExists(fileToMove));
	cout << "   b. No silent overwrite: fails if interactive." << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	createFileIfInexistent(movedFile);
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::moveFile(fileToMove, existingDir, false, false));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(FileMgr::fileExists(fileToMove));
#ifdef INCLUDE_INTERACTIVE_TESTS
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	createFileIfInexistent(movedFile);
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(!FileMgr::moveFile(fileToMove, existingDir, false, true));
	ASSERT(FileMgr::fileExists(movedFile));
	ASSERT(FileMgr::fileExists(fileToMove));
#endif

	cout << "5. move existent file into a file (should fail)" << endl;
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	// Create a dir which is in fact a file
	createFileIfInexistent(inexistentDir);
	ASSERT(FileMgr::fileExists(inexistentDir));
	ASSERT(!FileMgr::moveFile(fileToMove, inexistentDir, true, false));
	ASSERT(FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	// Create a dir which is in fact a file
	createFileIfInexistent(inexistentDir);
	ASSERT(FileMgr::fileExists(inexistentDir));
	ASSERT(!FileMgr::moveFile(fileToMove, inexistentDir, false, false));
	ASSERT(FileMgr::fileExists(fileToMove));
#ifdef INCLUDE_INTERACTIVE_TESTS
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	// Create a dir which is in fact a file
	createFileIfInexistent(inexistentDir);
	ASSERT(FileMgr::fileExists(inexistentDir));
	ASSERT(!FileMgr::moveFile(fileToMove, inexistentDir, true, true));
	ASSERT(FileMgr::fileExists(fileToMove));
	InitTestOfMoveFile(fileToMove, existingDir, inexistentDir, movedFile, inexistentFile);
	// Create a dir which is in fact a file
	createFileIfInexistent(inexistentDir);
	ASSERT(FileMgr::fileExists(inexistentDir));
	ASSERT(!FileMgr::moveFile(fileToMove, inexistentDir, true, false));
	ASSERT(FileMgr::fileExists(fileToMove));
#endif

	cout << "Cleaning up..." << endl;
	FileMgr::deleteFile(movedFile,true);
	FileMgr::deleteFile(fileToMove,true);
	if (FileMgr::fileExists(inexistentDir)) { // some tests created file instead of dir
		FileMgr::deleteFile(inexistentDir, true);
	}
}


cute::suite make_suite_TestFileMgr() {
	cute::suite s { };
	s.push_back(CUTE(testQueryFunctions));
	s.push_back(CUTE(testDirectoryCreationDeletion));
	s.push_back(CUTE(testDirectoryListing));
	s.push_back(CUTE(TestMoveFile));

	return s;
}
