#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include "DebugUtils.h"

// Add one include here for each test suite.
#include "TestFileMgr.h"
#include "TestRecordProcessor.h"
#include "TestCancellationWatch.h"

INITIALIZE_EASYLOGGINGPP

bool runSuite(int argc, char const *argv[]) {
	// Set up logging
	START_EASYLOGGINGPP(argc, argv);
	configureLogging();

	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = true;

	// Add 2 lines hereunder for each Test suite.
	cute::suite TestFileMgr = make_suite_TestFileMgr();
	success &= runner(TestFileMgr, "FileMgr_Test");
	cute::suite TestProcessor = make_suite_TestRecordProcessor();
	success &= runner(TestProcessor, "TestProcessor");
	cute::suite TestWatch = make_suite_InteractiveTests();
	success &= runner(TestWatch, "Interactive Tests");
	return success;
}

int main(int argc, char const *argv[]) {
    return runSuite(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
